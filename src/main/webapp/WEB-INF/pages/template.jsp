<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring-form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="./css/TC24.css">
	<title>BBVA - Visualizaci&oacute;n de res&uacute;menes</title>
</head>
<body>
	<div id="capa_madre">
		<div id="header">
			<div id="fecha">
				xx/xx/xxxx
			</div>
				<div id="user">Usuario:</div>
				<div id="userCuenta">
					Axxxxxx
				</div>
				<div id="ico">
					<a style="left: 10px" onclick="history.go(-1) ">
						<img src="./images/1314985508_back.png" alt="Volver" style="cursor: pointer;" />
					</a> &nbsp;&nbsp;&nbsp;&nbsp; 
						<img src="./images/1314986520_exit.png"	alt="Salir" style="cursor: pointer;" />
				</div>
		</div>
		<div id="sidebar">
			<div id="sidebarMenu">
				<c:if test="${usuario != null}">
					<ul>
						<li><spring:url value="panelBusqueda.do">
								<span>Panel General de
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B�squeda</span>
							</spring:url>
						</li>
					</ul>
					<ul>
						<li><spring:url value="/inicio.do">
								<span>Inicio</span>
							</spring:url>
						</li>
					</ul>
				</c:if>
			</div>
		</div>
		<div id="cuerpo_central_container">
			<div id="cuerpo_central">
				<div id="cuerpo_central_top">
					<img src='./images/search.png' alt="icono titulo" id="icono" />
					<h2>
						<tiles:getAsString name="titulo" />
					</h2>
				</div>
				<div id="cuerpo_central_content">
					<spring-form:errors />
					<tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>
		<div id="footer"></div>
	</div>
</body>
</html>