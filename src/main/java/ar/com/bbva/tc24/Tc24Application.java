package ar.com.bbva.tc24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tc24Application {

	public static void main(String[] args) {
		SpringApplication.run(Tc24Application.class, args);
	}

}
